#include <iostream>
#include <string>

using namespace std;

int main() 
{
	string Str = "";

	cout << "Enter yore string: ";
	getline(cin, Str);

	short Size = Str.length();

	cout << "String length: " << Size << "\n";
	cout << "First symbol: " << Str.operator[](0) << "\n";
	cout << "Last symbol: " << Str.operator[](Size - 1) << "\n";
}